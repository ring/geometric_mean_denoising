%% Author: Nicolas Gasnier
%% Copyright 2020 CS GROUP - France, Centre national d'études spatiales, Télécom Paris 
%% nicolas.gasnier@telecom-paris.fr

%% Adapted from the original RABASAR code written by Charles Deledalle (https://bitbucket.org/cdeledalle/rabasar)



%% Example of RABASAR modified to use a geometric mean superimage on a stack of 20 Sentinel-1 SLC from St Nazaire (France)
clear all
close all
set(0, 'DefaultAxesFontSize',30)
addpath(genpath('.'))

disp('Please note that this implementation uses DDID denoiser and not BM2D for copyright reasons. The results presented in the article have been obtained with BM3D denoiser. See README.md for more information')
% Load SAR time series
% v is a crop of a S1 SLC time series spatially undersampled by a factor 2
% to limit noise correlation

verbose = false; % Set to true to display more results


load('Example_S1_20_images_undersampled.mat')

v = v.*v; % Amplitude to intensity


t = 11;
L = 1;

% Rabasar
h = robustwaitbar(0);
tic
[u_hat_t, u_hat_si, L_hat_si, u_hat_dsi, L_hat_dsi] = ...
    rabasar_geom(v, L, t, ...
            'waitbar', @(p) robustwaitbar(p, h));
toc
close(h);
u_hat_t_g = u_hat_t;
u_hat_si_g = u_hat_si;
L_hat_si_g = L_hat_si;
u_hat_dsi_g = u_hat_dsi;
L_hat_dsi_g = L_hat_dsi;

%% Visualization
min1 = 0.1;
max1 = 10;
fancyfigure;
h1 = plotimagesar(u_hat_si_g, 'alpha', 2/3);
title(sprintf('Noisy Geometric mean'));
fancyfigure;
plotimagesar(u_hat_dsi_g, 'rangeof', h1);
title(sprintf('Denoised geometric mean'));
fancyfigure;
plotimagesar(v(:,:,t), 'rangeof', h1);
title(sprintf('Noisy image at date: t=%d', t));
fancyfigure;
plotimagesar(v(:,:,17), 'rangeof', h1);
title(sprintf('Noisy image at date: t=%d', 17));
fancyfigure;
plotimagesar(u_hat_t, 'rangeof', h1);
title(sprintf('Rabasar result (with geometric mean) at date: t=%d', t));

if verbose == true
    fancyfigure;
    imagesc(v(:,:,t)./u_hat_t,[min1,max1]);
    title(sprintf('Ratio noisy/denoised (with geometric mean) at date: t=%d', t));
    colormap gray
    fancyfigure;
    histogram(v(:,:,t)./u_hat_t,200,'BinLimits',[0.1,10]);
    title(sprintf('Ratio noisy/denoised (with geometric mean) at date: t=%d', t));
    linkaxes;
    mean(mean(v(:,:,t)./u_hat_t))
end

%%

tic

clear u_hat_t u_hat_si L_hat_si u_hat_dsi L_hat_dsi h
h = robustwaitbar(0);

[u_hat_t, u_hat_si, L_hat_si, u_hat_dsi, L_hat_dsi] = ...
    rabasar_arith(v, L, t, ...
            'waitbar', @(p) robustwaitbar(p, h));
toc
close(h);

u_hat_t_a = u_hat_t;
u_hat_si_a = u_hat_si;
L_hat_si_a = L_hat_si;
u_hat_dsi_a = u_hat_dsi;
L_hat_dsi_a = L_hat_dsi;

%% Visualization
min1 = 0.1;
max1 = 10;
fancyfigure;
plotimagesar(u_hat_si_a, 'rangeof', h1);
title(sprintf('Arithmetic mean'));
fancyfigure;
plotimagesar(u_hat_dsi_a, 'rangeof', h1);
title(sprintf('Denoised arithmetic mean'));
fancyfigure;
plotimagesar(u_hat_t, 'rangeof', h1);
title(sprintf('Rabasar result (with arithmetic mean) at date: t=%d', t));

if verbose == true
    fancyfigure;
    imagesc(v(:,:,t)./u_hat_t,[min1,max1]);
    title(sprintf('Ratio noisy/denoised (with arithmetic mean) at date: t=%d', t));
    colormap gray
    fancyfigure;
    histogram(v(:,:,t)./u_hat_t,200,'BinLimits',[0.1,10]);
    title(sprintf('Ratio noisy/denoised (with arithmetic mean) at date: t=%d', t));
    mean(mean(v(:,:,t)./u_hat_t))
end
%%


    min1 = 0.1;
    max1 = 7;
    
    fancyfigure;
    imagesc(u_hat_dsi_a./u_hat_dsi_g,[min1,max1]);
    title(sprintf('Ratio denoised arithmetic mean / denoised arithmetic mean'));
    colormap gray
    
    fancyfigure;
    imagesc(u_hat_si_a./u_hat_si_g,[min1,max1]);
    title(sprintf('Ratio noisy arithmetic mean / noisy geometric mean'));
    colormap gray
    


if verbose == true
    
    
    min1 = 0.3;
    max1 = 3;
    
    fancyfigure;
    imagesc(u_hat_t_a./u_hat_t_g,[min1,max1]);
    title(sprintf('Ratio arith/geom results at date: t=%d', t));
    colormap gray
    
    fancyfigure;
    imagesc(u_hat_t_g./u_hat_t_a,[min1,max1]);
    title(sprintf('Ratio geom/arith results at date: t=%d', t));
    colormap gray
    

    
    fancyfigure;
    histogram(u_hat_t_a./u_hat_t_g,200,'BinLimits',[0.1,10]);
    title(sprintf('Ratio arith/geom at date: t=%d', t));
    colormap gray
    
    fancyfigure;
    histogram(u_hat_dsi_a./u_hat_dsi_g,200,'BinLimits',[0.1,10]);
    title(sprintf('Ratio arith/geom dsi'));
    colormap gray
end
    linkaxes;
    
