function [D1, D2, vect_x] = Generate_tabulated_values_geom(y_val,vect_x,Lo_val,L_nb_im,step)

%% 
%% Author: Nicolas Gasnier
%% Copyright 2020 CS GROUP - France, Centre national d'études spatiales, Télécom Paris 
%% nicolas.gasnier@telecom-paris.fr

vecteur_y = double(f(y_val,vect_x,Lo_val)); % f is defined below

%%
TF_vecteur_y = fft(ifftshift(vecteur_y));

n = length(vect_x);
%%

TF_u  = TF_vecteur_y.^L_nb_im;

u = ifftshift(ifft(TF_u));

ratio = step^(L_nb_im-1);
u = u * ratio;



Fl = -log(u);

%% Finite difference derivation

D1 = zeros(1,n);
D1d = zeros(1,n);
D2 = zeros(1,n);



for i=1:n-1
    D1(i) = Fl(i+1) - Fl(i);
end
for i=2:n-1
    D2(i) = Fl(i+1) + Fl(i-1) - 2*Fl(i);
end

D1 = D1 / step;
D2 = D2 /(step*step);

end

%% Defining the function f which is Fisher-Tippett
function res = f(x,y,Lo)
res = (Lo^Lo/gamma(Lo)).*exp(Lo.*(y-x)).*exp(-Lo.*exp(y-x));
end
