function [R] = molog_geom(I, L, denoiser, varargin)


%% Author: Nicolas Gasnier
%% Copyright 2020 CS GROUP - France, Centre national d'études spatiales, Télécom Paris 
%% nicolas.gasnier@telecom-paris.fr

%% Adapted from the original RABASAR code written by Charles Deledalle (https://bitbucket.org/cdeledalle/rabasar)




%% Implements MoLoG (Monochannel simplification of MuLoG)
%
%    Monochannel version of MuLoG as described in
%
%    Deledalle, C.A., Denis, L., Tabti, S. and Tupin, F., 2017.
%    MuLoG, or How to apply Gaussian denoisers to multi-channel SAR
%    speckle reduction?
%
% Input/Output
%
%    I          a M x N array
%
%    R          estimated array
%
%    x          estimated log for R
%
%    y          extracted log for I
%s
%    L          the number of looks: parameter of the Gamma
%               distribution linking C and Sigma
%               For SLC images: L = 1
%               For MLC images: L = ENL
%
%    DENOISER   handle on a function x = DENOISER(y, sig, ...)
%               removing noise for an image y damaged by Gaussian
%               noise with varaince sig^2
%
%               extra arguments of MULOG are passed to DENOISER
%
% Optional arguments
%
%    BETA       inner parameter of ADMM
%               default: 1 + 2 / L
%
%    LAMBDA     regularization parameter
%               default: 1
%
%    T          the number of iterations of ADMM
%               default: 6
%
%    K          the number of iterations for the Newton descent
%               default: 10
%
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.




options     = makeoptions(varargin{:});

%% Input data format conversion
reshaped = false;
if ndims(I) == 4
    reshaped = true;
    [D, D, M, N] = size(I);
    if D ~= 1
        error('Molog is for Monochannel images only');
    end
    I = squeeze(I);
elseif ndims(I) ~= 2
    error(['I must be a M x N array']);
end
I = double(I);

%% Remove negative and zero entries (safeguard)
I(abs(I) <= 0) = min(abs(I(abs(I) > 0)));

%% Initialization is debiased in intensity
%   - only the initialization (helps to converge faster)
%   - the original I will be given for the data fidelity term
I_init = I * L / exp(psi(L));

%% Log-channel decomposition:
y  = log(I_init);
sigma = estimate_sigma(y, 1, L);
y  = y ./ sigma;

%% Definition du vecteur
Lo_val = 1;
y_val = 0;
step = 0.0001;
vecteur_x = -50:step:50;
n = length(vecteur_x);
L_nb_im = L;
[D1,D2,x_values] = Generate_tabulated_values_geom(y_val,vecteur_x,Lo_val,L_nb_im,step);

%% Define fidelity term
proxllk = @(x, I, lambda, varargin) ...
          proxfishtipp(x, I, lambda, L, sigma, y, varargin{:});

%% Run Plug-and-play ADMM
x = admm(I, ...
         denoiser, ...
         'beta', 10 + 1/L, ...
         'sig', 1, ...
         'init', y, ...
         'A1', D1, ...
         'A2', D2, ...
         'A3', x_values, ...
         'proxllk', proxllk, ...
         varargin{:});

%% Return to the initial representation:
R = exp(x * sigma);

%% Output data format conversion
if reshaped
    R = reshape(R, [1, 1, M, N]);
end

end

%%% Prox of the Wishart Fisher-Tippet log likelihood

% Split the input onto the different cores
function z = proxfishtipp(x, I, lambda, L, sigma, y, varargin)

options   = makeoptions(varargin{:});
cbwaitbar = getoptions(options, 'waitbar', @(dummy) []);
K         = getoptions(options, 'K', 100);
derivee_1 = getoptions(options, 'A1', NaN);
derivee_2 = getoptions(options, 'A2', NaN);
vecteur_x = getoptions(options, 'A3', NaN);
K = 100;

[M, N, P] = size(x);
NC = feature('Numcores');
q = ceil(M / NC);
parfor c = 1:NC
    idx = floor((c-1)*q)+1:min(floor(c*q), M);
    zs{c} = subproxfishtipp(x(idx, :), I(idx, :), ...
                            lambda, L, sigma, ...
                            y(idx, :), K, ...
                            derivee_1,derivee_2,vecteur_x,...
                            @(p) cbwaitbar((NC - c + p) / NC));
    cbwaitbar((NC-c+1) / NC);
end
z = zeros(M, N, P);
for c = 1:NC
    idx = floor((c-1)*q)+1:min(floor(c*q), M);
    z(idx, :, :) = zs{c};
end

end

% Main function
function z = subproxfishtipp(x, I, lambda, L, sigma, y, K,derivee_1,derivee_2,vecteur_x, cbwaitbar)

z = (x + y ./ lambda) ./ (1 + 1 ./ lambda);
Ilog = log(I);
for k = 1:K
    argument =  - sigma * z + Ilog;
    D11 =  interp1(vecteur_x,derivee_1,argument,'nearest');
    D21 = interp1(vecteur_x,derivee_2,argument,'nearest');
    z = z - ...
        (z - x + sigma * lambda * D11 )./ ...
        abs(1 + sigma * lambda * D21);

    cbwaitbar(k / K);
end

end


%%% Predict the noise standard deviation on each channel
function sigma = estimate_sigma(x, D, L)

var = psi(1, L);
sigma = sqrt(var);

end
