function [Sigma x y] = mulog(C, L, denoiser, varargin)
%% Implements MuLoG as described in
%
%    Deledalle, C.A., Denis, L., Tabti, S. and Tupin, F., 2017.
%    MuLoG, or How to apply Gaussian denoisers to multi-channel SAR
%    speckle reduction?
%
% Input/Output
%
%    C          a M x N array, OR
%               a M x N field of D x D covariance matrices
%               size D x D x M x N
%
%    Sigma      estimated array, OR field of covariane matrices
%
%    x          estimated log-channels for output Sigma
%
%    y          extracted log-channels for input C
%
%    L          the number of looks: parameter of the Wishart
%               distribution linking C and Sigma
%               For SLC images: L = 1
%               For MLC images: L = ENL
%
%    DENOISER   handle on a function x = DENOISER(y, sig, ...)
%               removing noise for an image y damaged by Gaussian
%               noise with varaince sig^2
%
%               extra arguments of MULOG are passed to DENOISER
%
% Optional arguments
%
%    BETA       inner parameter of ADMM
%               default: 1 + 2 / L
%
%    LAMBDA     regularization parameter
%               default: 1
%
%    T          the number of iterations of ADMM
%               default: 6
%
%    K          the number of iterations for the Newton descent
%               default: 10
%
%    R          rank of input covariance matrix [1-D]
%               default: min(L, D)
%
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle
% Email charles-alban.deledalle@math.u-bordeaux.fr



options     = makeoptions(varargin{:});

%% Input data format conversion
if ndims(C) == 2
    reshaped = true;
    D = 1;
    [M, N] = size(C);
    C = reshape(C, [1, 1, M, N]);
elseif ndims(C) == 4
    reshaped = false;
    [D, D, M, N] = size(C);
else
    error(['C must be a M x N field of D x D covariance matrices of ' ...
           'size DxDxMxN array']);
end
C = double(C);

%% Remove negative and zero entries from the diagonal (safeguard)
d = C(1:D, 1:D, :, :);
d(abs(d) <= 0) = min(abs(d(abs(d) > 0)));
C(1:D, 1:D, :, :) = d;

%% Diagonal loading (2 purposes)
%   - make the energy closer to be strictly convex
%   - be able to compute its logarithm
R = getoptions(options, 'R', min(L, D));
C = diagloading(C, min(R, L));

%% Initialization is debiased in intensity
%   - only the initialization (helps to converge faster)
%   - the original C will be given for the data fidelity term
C_init = C * L / exp(psi(L));

%% Log-channel decomposition: y = OmegaInv(log C)
[y, pca] = covariance_matrix_field_to_log_channels(C_init, L);

%% Define fidelity term
proxllk = @(x, C, lambda, varargin) ...
          proxfishtipp(x, C, lambda, L, D, pca, y, varargin{:});

%% Run Plug-and-play ADMM
x = admm(C, ...
         denoiser, ...
         'beta', 1 + 2/L, ...
         'sig', 1, ...
         'init', y, ...
         'proxllk', proxllk, ...
         varargin{:});

%% Return to the initial representation: Sigma = exp(Omega(x))
Sigma = log_channels_to_covariance_matrix_field(x, pca);

%% Output data format conversion
if reshaped
    Sigma = reshape(Sigma, [M, N]);
end

end

%%% Extract log channels from a given covariance matrix field
function [y, pca] = covariance_matrix_field_to_log_channels(C, L)

[D, D, M, N] = size(C);

% 1. Find the PCA parameters (A, b) decorrelating the channels
alpha  = getchannels(logmatrices(C), D);
alpha  = reshape(alpha, [M*N, D^2])';
pca.b  = mean(alpha, 2);
tmp    = bsxfun(@minus, alpha, pca.b);
[pca.A, pca.S, ~]  = svds(tmp / sqrt(M * N), D^2);
tmp    = pca.A' * tmp;

% 2. Estimate sigma on each channel (Phi)
pca.sigma = estimate_sigma(reshape(tmp, [D^2, M, N]), D, L);

% 3. Normalize to a noise variance of 1
y  = bsxfun(@times, tmp, 1 ./ pca.sigma);
y  = reshape(y', [M, N, D^2]);
clear tmp;

end

%%% Reconstruct a covariance matrix field from the given log channels
function Sigma = log_channels_to_covariance_matrix_field(x, pca)

[M, N, D] = size(x);
D     = sqrt(D);
x     = reshape(x, [M * N, D^2])';
tmp   = bsxfun(@times, x, pca.sigma);
alpha = bsxfun(@plus, pca.A * tmp, pca.b);
clear tmp;
alpha = reshape(alpha', [M, N, D^2]);
Sigma = expmatrices(getmatrices(alpha, D));

end

%%% Apply a function f with proper reshaping
function x = applyr(x, f, M, N, D)

x = reshape(x, [M * N, D^2])';
x = f(x);
x = reshape(x', [M, N, D^2]);

end

%%% Prox of the Wishart Fisher-Tippet log likelihood

% Split the input onto the different cores
function z = proxfishtipp(x, C, lambda, L, D, pca, y, varargin)

options   = makeoptions(varargin{:});
cbwaitbar = getoptions(options, 'waitbar', @(dummy) []);
K         = getoptions(options, 'K', 10);

[M, N, P] = size(x);
NC = feature('Numcores');
q = ceil(M / NC);
parfor c = 1:NC
    idx = floor((c-1)*q)+1:min(floor(c*q), M);
    zs{c} = subproxfishtipp(x(idx, :, :), C(:, :, idx, :), ...
                            lambda, L, D, pca, ...
                            y(idx, :, :), K, ...
                            @(p) cbwaitbar((NC - c + p) / NC));
    cbwaitbar((NC-c+1) / NC);
end
z = zeros(M, N, P);
for c = 1:NC
    idx = floor((c-1)*q)+1:min(floor(c*q), M);
    z(idx, :, :) = zs{c};
end

end

% Main function
function z = subproxfishtipp(x, C, lambda, L, D, pca, y, K, cbwaitbar)

[D, D, M, N] = size(C);
id = zeros(M, N, D * D);
id(:, :, 1:D) = 1;

Sigma   = diag(pca.sigma);
b       = pca.b;
A       = pca.A * Sigma;
Ap_id   = applyr(id, @(x) A' * x, M, N, D);

sigma   = reshape(pca.sigma, [1 1 size(pca.sigma, 1)]);
z = (x + y ./ lambda) ./ (1 + 1 ./ lambda);
for k = 1:K
    A_z_plus_b  = applyr(z, @(z) bsxfun(@plus, A * z, b), M, N, D);
    ezm05       = expmatrices(getmatrices(-A_z_plus_b / 2, D));
    ezm05yzm05  = mulmatrices(ezm05, C);
    ezm05yzm05  = mulmatrices(ezm05yzm05, ezm05);
    ezm05yzm05  = getmatrices_adj(ezm05yzm05, D);
    Ap_e        = applyr(ezm05yzm05, @(x) A' * x, M, N, D);
    z = z - ...
        (z - x + lambda * L * (Ap_id - Ap_e)) ./ ...
        abs(1 + lambda * L * abs(Ap_e));

    cbwaitbar(k / K);
end

end

%%% Extract the channels of a DxD hermitian matrix field
% Here the sqrt(2) makes getmatrices and getchannels
% adjoint and inverse of each others when ratio = 1.
% Moreover, it makes the standard deviation of the noise
% on off-diagonal elements similar to those of the diagonal.
function y = getchannels(x, D)

ratio = 1;
[D, D, M, N] = size(x);
y = zeros(M, N, D^2);
d = 1;
for k = 1:D
    y(:, :, d) = real(squeeze(x(k, k, :, :)));
    d = d + 1;
end
for k = 1:D
    for l = k+1:D
        y(:, :, d) = real(squeeze(x(k, l, :, :))) * sqrt(2) / ratio;
        d = d + 1;
        y(:, :, d) = imag(squeeze(x(k, l, :, :))) * sqrt(2) / ratio;
        d = d + 1;
    end
end

end

%%% Adjoint of getmatrices
function y = getmatrices_adj(x, D)

ratio = 1;
[D, D, M, N] = size(x);
y = zeros(M, N, D^2);
d = 1;
for k = 1:D
    y(:, :, d) = real(squeeze(x(k, k, :, :)));
    d = d + 1;
end
for k = 1:D
    for l = k+1:D
        y(:, :, d) = real(squeeze(x(k, l, :, :))) * sqrt(2) * ratio;
        d = d + 1;
        y(:, :, d) = imag(squeeze(x(k, l, :, :))) * sqrt(2) * ratio;
        d = d + 1;
    end
end

end

%%% Create a DxD hermitian matrix field form its channel decomposition
function y = getmatrices(x, D)

ratio = 1;
[M, N, K] = size(x);
y = zeros(D, D, M, N);
d = 1;
for k = 1:D
    y(k, k, :, :) = reshape(x(:, :, d), [1 1 M N]);
    d = d + 1;
end
for k = 1:D
    for l = (k+1):D
        y(k, l, :, :) = y(k, l, :, :) + ...
            reshape(x(:, :, d), [1 1 M N]) / sqrt(2) * ratio;
        y(l, k, :, :) = y(l, k, :, :) + ...
            reshape(x(:, :, d), [1 1 M N]) / sqrt(2) * ratio;
        d = d + 1;
        y(k, l, :, :) = y(k, l, :, :) + ...
            sqrt(-1) * reshape(x(:, :, d), [1 1 M N]) / sqrt(2) * ratio;
        y(l, k, :, :) = y(l, k, :, :) + ...
            -sqrt(-1) * reshape(x(:, :, d), [1 1 M N]) / sqrt(2) * ratio;
        d = d + 1;
    end
end

end

%%% Predict the noise standard deviation on each channel
function sigma = estimate_sigma(x, D, L)

var = 0;
for i = 0:D-1
    var = var + psi(1, max(D,L) - i);
end
if D == 1
    sigma = sqrt(var);
else
    sigma = stdmad(x);
end

end

%%% Estimate the noise standard deviation on each channel
function s = stdmad(x)

s = zeros(size(x, 1), 1);
for k = 1:size(x, 1)
    y = x(k, 1:2:(end-1), 1:2:(end-1)) - x(k, 2:2:end, 2:2:end);
    y = y(:) / sqrt(2);
    s(k) = 1.4826 * median(abs(y - median(y)));
end

end
