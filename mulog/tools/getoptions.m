function v = getoptions(options, name, v, varargin)
%% Retrieve options parameter


%%% Author: Nicolas Gasnier
%% Copyright 2020 CS GROUP - France, Centre national d'études spatiales, Télécom Paris 
%% nicolas.gasnier@telecom-paris.fr

name = lower(name);

if isfield(options, name)
    v = eval(['options.' name ';']);
end