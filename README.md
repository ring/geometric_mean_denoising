# Geometric_Mean_Denoising

Matlab implementation associated with the article "On the use and denoising of the temporal geometric mean for SAR time series", published in IEEE Geoscience and Remote sensing Letters by N.Gasnier, L.Denis and F.Tupin ([https://ieeexplore.ieee.org/document/9339951](https://ieeexplore.ieee.org/document/9339951))


Author: Nicolas Gasnier
contact : [nicolas.gasnier@telecom-paris.fr](mailto:nicolas.gasnier@telecom-paris.fr)

**Usage**

The main results of the article can be reproduced by running the `example.m` script.

The _t_-th image of a SAR intensity images stack `v` with an equivalent number of looks `L` can be denoised by using the following command in matlab:

`h = robustwaitbar(0);
[u_hat_t, u_hat_si, L_hat_si, u_hat_dsi, L_hat_dsi] = rabasar_geom(v, L, t,'waitbar', @(p) robustwaitbar(p, h));`

It will return `u_hat_t` the denoised image at date `t`, `u_hat_si` the geometric mean of the time series, `L_hat_si` the ENL of the geometric mean, `u_hat_dsi` the denoised geometric mean (superimage) and its estimated ENL `L_hat_dsi`.



**Using the BM3D denoiser**

Please note that because of copyright issues, we could not to release our code along with the BM3D implementation used for our experiments. A DDID denoiser is used instead and its results are differents. To use a BM3D denoiser, you can copy the content of the denoisers folder that can be found in the original mulog repository:[https://bitbucket.org/charles_deledalle/mulog/src/master/](url)) in mulog/denoisers and change the choice of denoiser by uncommenting line 90 and commenting line 91 in both rabasar/rabasar_arith.m and rabasar/rabasar_geom.m



**Copyright and license**

Copyright (C) Copyright 2020 CS GROUP - France, Centre national d'études spatiales, Télécom Paris.

The Geometric Mean Denoising software as a whole is distributed under the CeCILL licence, version 2.0. A copy of this license is available in the LICENSE file. 

All code snippets provided in the documentation, tutorials and examples (available in the Readme and in the example script) are intended to show how to use Geometric Mean Denoising and are not, strictly speaking, part of Geometric Mean Denoising project. In order to facilitate their reuse, they are released under the “Zero-Clause BSD” license (aka BSD 0-Clause or 0BSD). This is a permissive license that allows you to copy and paste this source code without preserving copyright notices.
